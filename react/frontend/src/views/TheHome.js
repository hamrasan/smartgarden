import { Container, CardDeck, Card, Button, Accordion } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import ModalSetSensorsTime from "../components/ModalSetSensorsTime";
import SensorsGarden from "./SensorsGarden";
import ErrorComponent from "../components/ErrorComponent";
import appContext from "../appContext";
import { useErrorHandler } from "react-error-boundary";


function TheHome() {
  const axios = require("axios");
  let context = useContext(appContext);
  const [minutes, setMinutes] = useState(context.user.measureSensors);
  const [modalShow, setModalShow] = useState(false);
  const [gardenId, setGardenId] = useState(null);
  const [gardens, setGardens] = useState([]);
  const [error, setError] = useState(false);
  const handleError = useErrorHandler();

   const fetchGardens = () => {
      axios({
        method: "get",
        withCredentials: true,
        url: "http://localhost:8080/garden/all",
      })
        .then((res) => {
          if ((res.status == 200)) {
            setGardens(res.data);
            console.log(res);
          }else throw Error(res.status);
        })
        .catch((error) => {
          handleError(error);
          console.error(error);
        });
    };

  const setRefreshData = () => {
    axios({
      method: 'post',
      url: 'http://localhost:8080/sensors/request/',
      withCredentials: true,
      headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
      },
      data: {
        minutes: minutes,
      }
    }).then(res => {
      console.log(res);
      context.updateUser();
    })
    .catch((error) => {
      handleError(error);
    });
  };

  const handleSubmit = (minutes) => {
    setRefreshData(minutes);
    setModalShow(false);
  };

  useEffect(() => {
    console.log(context.user);
    fetchGardens();
  }, [error]);

  return (
    <ErrorComponent onReset={ () => setError(true)}>
    <div>
      <Container fluid className="pt-3 w-75">
        <div className="d-flex justify-content-between">
          <div className="d-flex align-items-baseline">
            <h6>
              Interval merania je {minutes} min
            </h6>
            <Button
              variant="info"
              className="mb-4 ml-2"
              onClick={() => setModalShow(true)}
            >
              Zmeniť
            </Button>
          </div>
          <ModalSetSensorsTime
          show={modalShow}
          onHide={() => setModalShow(false)}
          title={"Zmeň časový interval merania senzorov"}
          bodyTitle={"Vyber časový interval v min:"}
          bodyText={"interval"}
          setMinutes={setMinutes}
          minutes={minutes}
          onSubmit={handleSubmit}
        />
        </div>

        {gardens.map(garden =>(
          <Accordion defaultActiveKey="1">
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="0">
              {garden.name}
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
              {<SensorsGarden gardenId={garden.id} gardenName={garden.name}></SensorsGarden>}
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
        ))}
      </Container>
    </div>
    </ErrorComponent>
  );
}

export default TheHome;
